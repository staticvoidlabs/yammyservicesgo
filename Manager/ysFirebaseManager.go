package ysmanager

import (
	"fmt"
	"io/ioutil"
	"log"

	"gopkg.in/maddevsio/fcm.v1"
)

var FirebaseAPIKey string

// InitFirebase initializes the Firebase subsystem.
func InitFirebase() {

	FirebaseAPIKey = "n/a"

	// Read FCM api key from local file.
	tmpFileContent, err := ioutil.ReadFile("fcmapikey.txt")
	if err != nil {

		fmt.Println("Error reading FCM api key from file: ", err)

		return
	}

	FirebaseAPIKey = string(tmpFileContent)
}

// SendBroadcastNotification sends a push notification to all registrered devices.
func SendBroadcastNotification() {

	c := fcm.NewFCM(FirebaseAPIKey)

	data := map[string]string{
		"msg": "Hello World!",
		"sum": "Happy Day",
	}

	tmpDevices := GetDevices()

	for _, d := range tmpDevices {

		tmpToken := d.Token
		//tmpToken = "fR6E6uTQT1Wv4vlk751hIj:APA91bEMY1EpbSDWu1qG2eoP0xSKvXm0EtTOYKvpGu5faxbxazeEJE_t0xa8Zy47ns3oaYNTpua3ArzXgOR_k3cCqYALJ02WeHyLKZbAO-VB2bEufTkxeGAJndeJmqYgll5-BPP8qPNS"

		response, err := c.Send(fcm.Message{
			Data:             data,
			RegistrationIDs:  []string{tmpToken},
			ContentAvailable: true,
			Priority:         fcm.PriorityHigh,
			Notification: fcm.Notification{
				Title: "Aufgepasst!",
				Body:  "Es sind neue Vorschläge verfügbar.",
			},
		})
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Status Code   :", response.StatusCode)
		fmt.Println("Success       :", response.Success)
		fmt.Println("Fail          :", response.Fail)
		fmt.Println("Canonical_ids :", response.CanonicalIDs)
		fmt.Println("Topic MsgId   :", response.MsgID)
	}

}
