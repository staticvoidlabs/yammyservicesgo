package ysmanager

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	Models "../Models"
)

var mDevices []Models.Device

func InitDevices() {

	getDevicesByFile()

}

func GetDevices() []Models.Device {

	return mDevices
}

func SaveDeviceListToFile() {

	file, err := os.OpenFile("devices.txt", os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	for _, d := range mDevices {

		tmpLineToWrite := d.Token + "_____" + d.ID
		_, err := fmt.Fprintln(file, tmpLineToWrite)
		if err != nil {
			fmt.Println("Error writing device list: ", err)
		}
	}

	file.Close()

}

func getDevicesByFile() {

	/*
		mDevices = []string{"aaaE6uTQT1Wv4vlk751hIj:APA91bEMY1EpbSDWu1qG2eoP0xSKvXm0EtTOYKvpGu5faxbxazeEJE_t0xa8Zy47ns3oaYNTpua3ArzXgOR_k3cCqYALJ02WeHyLKZbAO-VB2bEufTkxeGAJndeJmqYgll5-BPP8qPNS_____aaa944de-0c67-48eb-89bf-935aee5e8dcb",
			"bbbE6uTQT1Wv4vlk751hIj:APA91bEMY1EpbSDWu1qG2eoP0xSKvXm0EtTOYKvpGu5faxbxazeEJE_t0xa8Zy47ns3oaYNTpua3ArzXgOR_k3cCqYALJ02WeHyLKZbAO-VB2bEufTkxeGAJndeJmqYgll5-BPP8qPNS_____bbb944de-0c67-48eb-89bf-935aee5e8dcb",
			"cccE6uTQT1Wv4vlk751hIj:APA91bEMY1EpbSDWu1qG2eoP0xSKvXm0EtTOYKvpGu5faxbxazeEJE_t0xa8Zy47ns3oaYNTpua3ArzXgOR_k3cCqYALJ02WeHyLKZbAO-VB2bEufTkxeGAJndeJmqYgll5-BPP8qPNS_____ccc944de-0c67-48eb-89bf-935aee5e8dcb",
			"dddE6uTQT1Wv4vlk751hIj:APA91bEMY1EpbSDWu1qG2eoP0xSKvXm0EtTOYKvpGu5faxbxazeEJE_t0xa8Zy47ns3oaYNTpua3ArzXgOR_k3cCqYALJ02WeHyLKZbAO-VB2bEufTkxeGAJndeJmqYgll5-BPP8qPNS_____ddd944de-0c67-48eb-89bf-935aee5e8dcb",
		}
	*/

	var tmpDevicesRaw []string
	var tmpDevicesClean []Models.Device

	// Read devices from local file.
	tmpFileContent, err := os.Open("devices.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer tmpFileContent.Close()

	scanner := bufio.NewScanner(tmpFileContent)
	for scanner.Scan() {
		tmpDevicesRaw = append(tmpDevicesRaw, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Iterate through device items and split them into device struct objects.
	for _, dr := range tmpDevicesRaw {

		tmpCurrentDeviceRawSplitted := strings.Split(string(dr), "_____")
		tmpCurrentDeviceClean := Models.Device{ID: tmpCurrentDeviceRawSplitted[1], Token: tmpCurrentDeviceRawSplitted[0]}

		tmpDevicesClean = append(tmpDevicesClean, tmpCurrentDeviceClean)
	}

	mDevices = tmpDevicesClean
}

// InsertOrUpdateToken adds or updates a device token in/to the list of known devices.
func InsertOrUpdateToken(deviceToken string) {

	tmpIsUpdate := false
	tmpNewDeviceTokenSplitted := strings.Split(string(deviceToken), "_____")

	// Check if device list contains new device.
	for i, d := range mDevices {

		// Update device token in list of known devices.
		if d.ID == tmpNewDeviceTokenSplitted[1] {
			mDevices[i].Token = tmpNewDeviceTokenSplitted[0]
			tmpIsUpdate = true
			WriteLogMessage("DeviceManager:InsertOrUpdateToken > Updated token for device: "+tmpNewDeviceTokenSplitted[1], false)
		}

	}

	// Add new device token to list of known devices.
	if !tmpIsUpdate {

		tmpNewDevice := Models.Device{ID: tmpNewDeviceTokenSplitted[1], Token: tmpNewDeviceTokenSplitted[0]}
		mDevices = append(mDevices, tmpNewDevice)

		WriteLogMessage("DeviceManager:InsertOrUpdateToken > Added token for device: "+tmpNewDeviceTokenSplitted[1], false)
	}

	SaveDeviceListToFile()

}
