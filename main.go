package main

import (
	Manager "./Manager"
	//Models "./Models"
)

func main() {

	Manager.InitFirebase()
	Manager.InitDevices()

	Manager.StartAPIService()

	/*
		data := map[string]string{
			"msg": "Hello World!",
			"sum": "Happy Day",
		}

		c := fcm.NewFCM("AAAAgGmMLjQ:APA91bHv3LqRueUkicHoXJsUWKO20pb40x09kTfd_zuYljuogdjDVkl0qKTr3vBJ8YXrU7QQ0IfOXd8Av86wJ8hfE6ZfrHY_19_tlxXy5r9GKBAG3lOdqlPUYDZNrGkBTH8nurkLrDDS")
		token := "fR6E6uTQT1Wv4vlk751hIj:APA91bEMY1EpbSDWu1qG2eoP0xSKvXm0EtTOYKvpGu5faxbxazeEJE_t0xa8Zy47ns3oaYNTpua3ArzXgOR_k3cCqYALJ02WeHyLKZbAO-VB2bEufTkxeGAJndeJmqYgll5-BPP8qPNS"

		// Emulator Pixel 3a with Store > fR6E6uTQT1Wv4vlk751hIj:APA91bEMY1EpbSDWu1qG2eoP0xSKvXm0EtTOYKvpGu5faxbxazeEJE_t0xa8Zy47ns3oaYNTpua3ArzXgOR_k3cCqYALJ02WeHyLKZbAO-VB2bEufTkxeGAJndeJmqYgll5-BPP8qPNS

		response, err := c.Send(fcm.Message{
			Data:             data,
			RegistrationIDs:  []string{token},
			ContentAvailable: true,
			Priority:         fcm.PriorityHigh,
			Notification: fcm.Notification{
				Title: "Yammy App",
				Body:  "Es sind schon wieder neue Vorschläge verfügbar!",
			},
		})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Status Code   :", response.StatusCode)
		fmt.Println("Success       :", response.Success)
		fmt.Println("Fail          :", response.Fail)
		fmt.Println("Canonical_ids :", response.CanonicalIDs)
		fmt.Println("Topic MsgId   :", response.MsgID)
	*/

}
