package ysmanager

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// StartAPIService starts the API service.
func StartAPIService() {

	WriteLogMessage("Starting Web API services", true)

	// Define routes and actions.
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", getServerStateInfo)
	//router.HandleFunc("/fcm", sendBroadcastNotification)
	router.HandleFunc("/fcm/{token}", insertOrUpdateToken).Methods("GET")

	// Start rest service.
	go log.Fatal(http.ListenAndServe(":9100", router))

}

// Endpoint implementation.
func getServerStateInfo(w http.ResponseWriter, r *http.Request) {

	logMessages := GetCurrentLog()

	for _, message := range logMessages {
		w.Write([]byte(message + "\r\n"))
	}

}

func sendBroadcastNotification(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:sendBroadcastNotification", true)

	SendBroadcastNotification()
}

func insertOrUpdateToken(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:insertOrUpdateToken", true)

	tmpToken := mux.Vars(r)["token"]

	InsertOrUpdateToken(tmpToken)

	json.NewEncoder(w).Encode("YammyService response: Set/updated token successful.")
}
